var LaCollection = artifacts.require("LaCollection");

module.exports = async function (deployer, network, accounts) {
  await deployer.deploy(LaCollection);
  const instanceLCA = await LaCollection.deployed();
  instanceLCA.addMinterRole(accounts[0]);
};