const { accounts, contract, web3 } = require('@openzeppelin/test-environment');
const { constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const LaCollection = contract.fromArtifact("LaCollection");

describe("Contract Migration", () => { });

describe("Creation and Mint Artwork", () => {
  const [owner, user, user2] = accounts;
  const toBN = web3.utils.toBN;

  beforeEach(async () => {
    this.LaCollection = await LaCollection.new({ from: owner });
    const tx = await this.LaCollection.addMinterRole(user2, { from: owner });
    expectEvent(tx, "RoleGranted", {
      role: "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6",
      account: user2,
      sender: owner
    });
  });

  describe("Create artwork (with max 3 editions) and mint 1 edition", () => {
    beforeEach(async () => {
      const tx = await this.LaCollection.createArtworkAndMintEdition(1000, 1, "tokenUri", 3, true, user, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(1000),
        scarcity: toBN(1)
      });
      expectEvent(tx, "ArtworkEditionMinted", {
        tokenId: toBN(1000),
        artworkNumber: toBN(1000),
        buyer: user
      });
      const getArtworkTx = await this.LaCollection.getArtwork(1000);
      expect(getArtworkTx.scarcity.toString()).to.equal('1');
      expect(getArtworkTx.totalMinted.toString()).to.equal('1');
      expect(getArtworkTx.totalLeft.toString()).to.equal('2');
    });

    it("should mint a new edition #1001 for artwork 1000", async () => {
      const txMint = await this.LaCollection.mint(user, 1000, { from: user2 });
      expectEvent(txMint, "ArtworkEditionMinted", {
        tokenId: toBN(1001),
        artworkNumber: toBN(1000),
        buyer: user
      });
      const getArtworkTx = await this.LaCollection.getArtwork(1000);
      expect(getArtworkTx.scarcity.toString()).to.equal('1');
      expect(getArtworkTx.totalMinted.toString()).to.equal('2');
      expect(getArtworkTx.totalLeft.toString()).to.equal('1');
    });

    it("should mint all editions availables (#1000, #1001, #1002", async () => {
      const txMint = await this.LaCollection.mint(user, 1000, { from: user2 });
      expectEvent(txMint, "ArtworkEditionMinted", {
        tokenId: toBN(1001),
        artworkNumber: toBN(1000),
        buyer: user
      });
      const txMint1 = await this.LaCollection.mint(user, 1000, { from: user2 });
      expectEvent(txMint1, "ArtworkEditionMinted", {
        tokenId: toBN(1002),
        artworkNumber: toBN(1000),
        buyer: user
      });

      await expectRevert(
        this.LaCollection.mint(user, 1000, { from: user2 }),
        "LaCollection: No more editions left to mint")
    });

    it("should prevent to create an artwork with number lower than last artwork number (1000) plus edition number (3)", async () => {
      await expectRevert(
        this.LaCollection.createArtworkAndMintEdition(1003, 1, "", 3, true, user, { from: user2 }),
        "LaCollection: Artwork number must be greater than previously used")
    })
  });
});