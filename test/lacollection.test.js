const { accounts, contract, web3 } = require('@openzeppelin/test-environment');
const { constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

const { expect } = require('chai');

const LaCollection = contract.fromArtifact("LaCollection");

describe("LaCollection", () => {
  const [owner, user, user2] = accounts;
  const toBN = web3.utils.toBN;

  beforeEach(async () => {
    this.LaCollection = await LaCollection.new({ from: owner });
    const tx = await this.LaCollection.addMinterRole(user2, { from: owner });
    expectEvent(tx, "RoleGranted", {
      role: "0x9f2df0fed2c77648de5860a4cc508cd0818c85b8b8a1ab4ceeef8d981c8956a6",
      account: user2,
      sender: owner
    });

  });

  it('the deployer is the owner', async () => {
    expect(await this.LaCollection.owner()).to.equal(owner);
  });

  describe("#createArtwork", () => {
    it("should prevent to create artwork a non minter address", async () => {
      await expectRevert(
        this.LaCollection.createArtwork(12321, 1, "", 3, true, { from: user }),
        "LaCollection: Caller is not a minter")
    });

    it("should prevent to create an artwork with serialNumber below 1", async () => {
      await expectRevert(
        this.LaCollection.createArtwork(0, 1, "", 3, true, { from: user2 }),
        "LaCollection: Artwork number not provided or must be greater than 1")
    });

    it("should prevent to create same artwork", async () => {
      await this.LaCollection.createArtwork(10000, 1, "htttt", 3, true, { from: user2 });
      await this.LaCollection.createArtwork(20000, 1, "htttt", 3, true, { from: user2 });

      await expectRevert(
        this.LaCollection.createArtwork(10000, 1, "", 3, true, { from: user2 }),
        "LaCollection: Artwork number must be greater than previously used");
    });

    it("should prevent to create artwork lower than highest artwork number", async () => {
      await this.LaCollection.createArtwork(30000, 1, "htttt", 3, true, { from: user2 });
      await this.LaCollection.createArtwork(50000, 1, "htttt", 3, true, { from: user2 });

      await expectRevert(
        this.LaCollection.createArtwork(40000, 1, "htttt", 3, true, { from: user2 }),
        "LaCollection: Artwork number must be greater than previously used");
    });

    it("should prevent to create artwork without metadata uri", async () => {
      await expectRevert(
        this.LaCollection.createArtwork(60000, 1, "", 3, true, { from: user2 }),
        "LaCollection: Token URI is missing");
    })

    it("should prevent to create artwork using an id already used by an edition", async () => {
      // Create from 1 
      await this.LaCollection.createArtwork(1, 1, "tokenUri", 3, true, { from: user2 });
      await expectRevert(
        this.LaCollection.createArtwork(3, 1, "tokenUri", 3, true, { from: user2 }),
        "LaCollection: Artwork number must be greater than previously used"
      );
    })
  });

  /**
   * # createArtworkAndMintEdition
   * uint256 _artworkNumber,
   * uint16 _scarcity,
   * string memory _tokenUri,
   * uint16 _totalLeft,
   * bool _active,
   * address _to
   */
  describe("#createArtworkAndMintEdition", () => {
    it("should prevent to create artwork and mint an edition to a non minter address", async () => {
      await expectRevert(
        this.LaCollection.createArtworkAndMintEdition(12321, 1, "", 3, true, user2, { from: user }),
        "LaCollection: Caller is not a minter")
    });

    it("should prevent to create an artwork with serialNumber below 1", async () => {
      await expectRevert(
        this.LaCollection.createArtworkAndMintEdition(0, 1, "", 3, true, user, { from: user2 }),
        "LaCollection: Artwork number not provided or must be greater than 1")
    });

    it("should prevent to mint more edition than expected", async () => {
      // Create from 1 
      const tx = await this.LaCollection.createArtworkAndMintEdition(1, 1, "tokenUri", 1, true, user, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(1),
        scarcity: toBN(1)
      });
      expectEvent(tx, "ArtworkEditionMinted", {
        tokenId: toBN(1),
        artworkNumber: toBN(1),
        buyer: user
      });
      await expectRevert(
        this.LaCollection.mint(user, 1, { from: user2 }),
        "LaCollection: No more editions left to mint"
      )
    });

    it("should prevent to create & mint an existing artworr", async () => {
      await this.LaCollection.createArtworkAndMintEdition(10000, 1, "htttt", 3, true, user, { from: user2 });
      await this.LaCollection.createArtworkAndMintEdition(20000, 1, "htttt", 3, true, user, { from: user2 });

      await expectRevert(
        this.LaCollection.createArtwork(10000, 1, "", 3, true, { from: user2 }),
        "LaCollection: Artwork number must be greater than previously used");
    });

    it("should create an artwork (#1000) with 3 editions and mint one", async () => {
      const tx = await this.LaCollection.createArtworkAndMintEdition(1000, 1, "tokenUri", 3, true, user, { from: user2 });

      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(1000),
        scarcity: toBN(1)
      });
      expectEvent(tx, "ArtworkEditionMinted", {
        tokenId: toBN(1000),
        artworkNumber: toBN(1000),
        buyer: user
      });

    });
  });

  describe("#mint", () => {
    it("should prevent to mint a non minter address", async () => {
      // Create an artwork
      await this.LaCollection.createArtwork(100000, 1, "htttt", 3, true, { from: user2 });

      await expectRevert(
        this.LaCollection.mint(user, 100000, { from: user }),
        "LaCollection: Caller is not a minter")
    });

    it("should prevent to mint if contract is in pause", async () => { })

    it("should prevent to mint if no edition left to mint", async () => { })

    it("should prevent to mint a unknown artwork", async () => {
      // Create an artwork
      await this.LaCollection.createArtwork(120000, 1, "htttt", 3, true, { from: user2 });

      await expectRevert(
        this.LaCollection.mint(user, 120001, { from: user2 }),
        "LaCollection: No more editions left to mint")
    });

    it("should mint an existing artwork", async () => {
      // Create an artwork
      const tx = await this.LaCollection.createArtwork(130000, 1, "htttt", 3, true, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(130000),
        scarcity: toBN(1)
      });
      const txMint = await this.LaCollection.mint(user, 130000, { from: user2 });
      expectEvent(txMint, "ArtworkEditionMinted", {
        tokenId: toBN(130000),
        artworkNumber: toBN(130000),
        buyer: user
      });
    });

    it("should mint only 1 existing artwork if scarcity is unique", async () => {
      // Create an artwork
      const tx = await this.LaCollection.createArtwork(140000, 1, "htttt", 1, true, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(140000),
        scarcity: toBN(1)
      });
      const txMint = await this.LaCollection.mint(user, 140000, { from: user2 });
      expectEvent(txMint, "ArtworkEditionMinted", {
        tokenId: toBN(140000),
        artworkNumber: toBN(140000),
        buyer: user
      });

      await expectRevert(
        this.LaCollection.mint(user, 140000, { from: user2 }),
        "LaCollection: No more editions left to mint")
    });
  });

  describe("#getArtwork", () => {
    it("should return an empty artwork details from unidentified artwork number", async () => {
      const getArtworkTx = await this.LaCollection.getArtwork(130000);
      expect(getArtworkTx.scarcity.toString()).to.equal('0');
      expect(getArtworkTx.totalMinted.toString()).to.equal('0');
      expect(getArtworkTx.totalLeft.toString()).to.equal('0');
      expect(getArtworkTx.tokenUri).to.equal('');
      expect(getArtworkTx.active).to.equal(false);
    });

    it("should return artwork details from artworkNumber if exists", async () => {
      const tx = await this.LaCollection.createArtwork(130000, 1, "htttt", 3, true, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(130000),
        scarcity: toBN(1)
      });
      const txMint = await this.LaCollection.mint(user, 130000, { from: user2 });
      expectEvent(txMint, "ArtworkEditionMinted", {
        tokenId: toBN(130000),
        artworkNumber: toBN(130000),
        buyer: user
      });

      const getArtworkTx = await this.LaCollection.getArtwork(130000);
      expect(getArtworkTx.scarcity.toString()).to.equal('1');
      expect(getArtworkTx.totalMinted.toString()).to.equal('1');
      expect(getArtworkTx.totalLeft.toString()).to.equal('2');
      expect(getArtworkTx.tokenUri).to.equal('htttt');
      expect(getArtworkTx.active).to.equal(true);
    })
  })

  describe("#getArtworkNumber", () => {
    it("should return an 0 from unidentified tokenId", async () => {
      const getArtworkTx = await this.LaCollection.getArtworkNumber(130000);
      expect(getArtworkTx.toString()).to.equal('0');
    });

    it("should return artwork details from artworkNumber if exists", async () => {
      const tx = await this.LaCollection.createArtwork(130000, 1, "htttt", 3, true, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(130000),
        scarcity: toBN(1)
      });
      await this.LaCollection.mint(user, 130000, { from: user2 });
      await this.LaCollection.mint(user, 130000, { from: user2 });

      const getArtworkTx = await this.LaCollection.getArtworkNumber(130000);
      expect(getArtworkTx.toString()).to.equal('130000');
      const getArtworkTx1 = await this.LaCollection.getArtworkNumber(130001);
      expect(getArtworkTx1.toString()).to.equal('130000');
    })
  })

  describe("#getTokenIds", () => {
    it("should return an empty array for unidentified artworkNumber", async () => {
      const getArtworkTx = await this.LaCollection.getTokenIds(130000);
      expect(getArtworkTx.length).to.equal(0);
    });

    it("should return artwork details from artworkNumber if exists", async () => {
      const tx = await this.LaCollection.createArtwork(130000, 1, "htttt", 3, true, { from: user2 });
      expectEvent(tx, "ArtworkCreated", {
        artworkNumber: toBN(130000),
        scarcity: toBN(1)
      });
      await this.LaCollection.mint(user, 130000, { from: user2 });
      const getArtworkTx = await this.LaCollection.getTokenIds(130000);
      expect(getArtworkTx.length).to.equal(1);
      expect(getArtworkTx).to.eql([toBN(130000)]);

      await this.LaCollection.mint(user, 130000, { from: user2 });
      const getArtworkTx1 = await this.LaCollection.getTokenIds(130000);
      expect(getArtworkTx1.length).to.equal(2);
      expect(getArtworkTx1).to.eql([toBN(130000), toBN(130001)]);

    })
  });
});