# LaCollection Smart-Contract

## Overview

This smart contract was created to used in LaCollection.io project to mint Artwork NFT.

The smart contract use the openzeppelin implementation of standard [ERC721](https://docs.openzeppelin.com/contracts/erc721).


## Project Install

1. Clone this project locally
2. Run `yarn install` in your bash


## Test Environnement

* Test runner: Mocha
* `@truffle/contract` as contract abstraction
* `@openzeppelin/test-helpers` & `Chai`as assertion library

### Build smart-contract and run unit tests
```console
yarn test
```
### Run only unit tests
```shell
yarn test:only
```
